// var CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");

module.exports = {
    entry: {
       index: "./js/index.js",
       rgl: "./js/rgl.js",
        comments: "./js/comments.js"
    },
    
    output: {
        path: __dirname + "/built",
        publicPath: "/chartsDnD/built/",
        filename: "[name].bundle.js"
    },
    
    module: {
        loaders: [
            { test: /\.css$/, loader: "style!css" },
            { test: /\.less$/, loader: "style!css!less" },
            { test: /\.(png|jpg)$/, loader: 'url-loader?limit=10000' },
            { test: /\.js$/, exclude: /(node_modules|bower_components)/, loader: 'babel?presets[]=es2015' },
            { test: require.resolve("jquery"), loader: "expose?$!expose?jQuery" }
        ]
    }

/*    plugins: [
        new CommonsChunkPlugin("admin-commons.js", ["ap1", "ap2"]),
        new CommonsChunkPlugin("commons.js", ["p1", "p2", "admin-commons.js"])
    ]*/
};