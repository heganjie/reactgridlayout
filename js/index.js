import '../styles/index.less';
//import 'expose?$!expose?jQuery!jquery';
import React from 'react';
import ReactDom from 'react-dom';
import {Responsive, WidthProvider} from 'react-grid-layout';
const ResponsiveReactGridLayout = WidthProvider(Responsive);
import 'react-grid-layout/css/styles.css'
import 'react-resizable/css/styles.css'
import ReactEcharts from 'echarts-for-react'

var Chart1 = React.createClass({
    getOption: function() {
        const option = {
            title: {
                text: '堆叠区域图'
            },
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:['邮件营销','联盟广告','视频广告']
            },
            toolbox: {
                feature: {
                    dataView: {readOnly: true},
                    restore: {},
                    saveAsImage: {}
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : ['周一','周二','周三','周四','周五','周六','周日']
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : [
                {
                    name:'邮件营销',
                    type:'line',
                    stack: '总量',
                    areaStyle: {normal: {}},
                    data:[120, 132, 101, 134, 90, 230, 210]
                },
                {
                    name:'联盟广告',
                    type:'line',
                    stack: '总量',
                    areaStyle: {normal: {}},
                    data:[220, 182, 191, 234, 290, 330, 310]
                },
                {
                    name:'视频广告',
                    type:'line',
                    stack: '总量',
                    areaStyle: {normal: {}},
                    data:[150, 232, 201, 154, 190, 330, 410]
                }
            ]
        };
        return option;
    },
    render: function() {
        return (
            <ReactEcharts
                option={this.getOption()}
                style={{height: '100%', width: '100%'}}
                className='react_for_echarts' />
        );
    }
});

/*var Chart2 = React.createClass({
    getOption: function () {
        var option = {
            backgroundColor: new echarts.graphic.RadialGradient(0.3, 0.3, 0.8, [{
                offset: 0,
                color: '#f7f8fa'
            }, {
                offset: 1,
                color: '#cdd0d5'
            }]),
            title: {
                text: '1990 与 2015 年各国家人均寿命与 GDP'
            },
            legend: {
                right: 10,
                data: ['1990', '2015']
            },
            xAxis: {
                splitLine: {
                    lineStyle: {
                        type: 'dashed'
                    }
                }
            },
            yAxis: {
                splitLine: {
                    lineStyle: {
                        type: 'dashed'
                    }
                },
                scale: true
            },
            series: [{
                name: '1990',
                data: data[0],
                type: 'scatter',
                symbolSize: function (data) {
                    return Math.sqrt(data[2]) / 5e2;
                },
                label: {
                    emphasis: {
                        show: true,
                        formatter: function (param) {
                            return param.data[3];
                        },
                        position: 'top'
                    }
                },
                itemStyle: {
                    normal: {
                        shadowBlur: 10,
                        shadowColor: 'rgba(120, 36, 50, 0.5)',
                        shadowOffsetY: 5,
                        color: new echarts.graphic.RadialGradient(0.4, 0.3, 1, [{
                            offset: 0,
                            color: 'rgb(251, 118, 123)'
                        }, {
                            offset: 1,
                            color: 'rgb(204, 46, 72)'
                        }])
                    }
                }
            }, {
                name: '2015',
                data: data[1],
                type: 'scatter',
                symbolSize: function (data) {
                    return Math.sqrt(data[2]) / 5e2;
                },
                label: {
                    emphasis: {
                        show: true,
                        formatter: function (param) {
                            return param.data[3];
                        },
                        position: 'top'
                    }
                },
                itemStyle: {
                    normal: {
                        shadowBlur: 10,
                        shadowColor: 'rgba(25, 100, 150, 0.5)',
                        shadowOffsetY: 5,
                        color: new echarts.graphic.RadialGradient(0.4, 0.3, 1, [{
                            offset: 0,
                            color: 'rgb(129, 227, 238)'
                        }, {
                            offset: 1,
                            color: 'rgb(25, 183, 207)'
                        }])
                    }
                }
            }]
        };
        return option;
    },
    render: function () {
        return (
            <ReactEcharts
                option={this.getOption()}
                style={{height: '100%', width: '100%'}}
                className='react_for_echarts' />
        );
    }
});*/

var MyFirstGrid = React.createClass({
    getInitialState: function(){
        return {
            layouts: {
                lg: [
                    {i: 'a', x: 0, y: 0, w: 4, h: 2},
                    {i: 'b', x: 4, y: 0, w: 4, h: 2},
                    {i: 'c', x: 8, y: 0, w: 2, h: 2}
                ],
                md: [
                    {i: 'a', x: 0, y: 0, w: 3, h: 2},
                    {i: 'b', x: 3, y: 0, w: 2, h: 2},
                    {i: 'c', x: 5, y: 0, w: 5, h: 2}
                ],
                sm: [
                    {i: 'a', x: 0, y: 0, w: 1, h: 2},
                    {i: 'b', x: 1, y: 0, w: 2, h: 2},
                    {i: 'c', x: 3, y: 0, w: 1, h: 2}
                ],
                xs: [
                    {i: 'a', x: 0, y: 0, w: 2, h: 2},
                    {i: 'b', x: 2, y: 0, w: 2, h: 2},
                    {i: 'c', x: 0, y: 4, w: 2, h: 2}
                ],
                xxs: [
                    {i: 'a', x: 0, y: 0, w: 2, h: 2},
                    {i: 'b', x: 0, y: 2, w: 2, h: 2},
                    {i: 'c', x: 0, y: 4, w: 2, h: 2}
                ]
            }
        }
    },
    render: function(){
        var {layouts} = this.state;
        return (
            <ResponsiveReactGridLayout className='layout' layouts={layouts}
                                       breakpoints={{lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0}}
                                       cols={{lg: 12, md: 10, sm: 6, xs: 4, xxs: 2}}>
                <div key={'a'} className='box1'><Chart1 /></div>
                <div key={'b'} className='box2'></div>
                <div key={'c'} className='box3'></div>
            </ResponsiveReactGridLayout>
        )
    }
});

ReactDom.render(<MyFirstGrid />, document.getElementById('renderBox'));


