import "expose?$!expose?jQuery!jquery"
import React from "react"
import ReactDom from "react-dom"

$(function () {
    var Content = React.createClass({
        getInitialState: function () {
            return {
                message: "",
                messageList: []
            }
        },
        handleChange: function (ev) {
            this.setState({
                message: ev.target.value
            });
        },
        handleSubmit: function () {
            this.setState({
                message: "",
                messageList: [(this.props.selectName ? ("@" + this.props.selectName) : "") + "\n" + this.state.message].concat(this.state.messageList)
            }, () => this.props.onSubmit())
        },
        render: function () {
            return <div>
                    <textArea  placeholder="please write a message..." value={this.state.message} onChange={this.handleChange}> </textArea>
                    <button type="submit" onClick={this.handleSubmit}>submit</button>
                    <ul>
                        {this.state.messageList.map((item, i) => <li key={i}>{item}</li>)}
                    </ul>
            </div>
        },
        componentDidMount: function () {
            
        }
    });
    
    var Comment = React.createClass({
        getInitialState: function () {
            return {
                users: ['', "Tom", "Hans", "Miu"],
                selectName: ""
            }
        },
        handleSelect: function (ev) {
            this.setState({
                selectName: ev.target.value
            });
        },
        render: function () {
            return <div>
                <select name="users" id="users" onChange={this.handleSelect} value={this.state.selectName}>
                    {this.state.users.map((user,i) => <option key={i}>{user}</option>)}
                </select>
                    <Content selectName={this.state.selectName} onSubmit={() => this.setState({selectName: ''})} />
                </div>
        }
    });
    
    ReactDom.render(<Comment />, document.getElementById("commentContainer"));
});
