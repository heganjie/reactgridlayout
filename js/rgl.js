import '../styles/index.less';
import 'expose?$!expose?jQuery!jquery';
import 'highcharts';
import React from "react";
import ReactDom from 'react-dom';
var ReactGridLayout = require('react-grid-layout');
import 'react-grid-layout/css/styles.css'
import 'react-resizable/css/styles.css'
var elementResizeEvent = require('element-resize-event');
const ReactHighcharts = require('react-highcharts');


var SecondGrid = React.createClass({
    componentDidMount: function() {
        elementResizeEvent(this.refs.chartContainer, () => {
            this.refs.chart.getChart().reflow();
        })
    },
    render: function () {
        var layout = [
            {i: '1', x: 0, y: 0, w: 1, h: 2},
            {i: '2', x: 1, y: 0, w: 3, h: 2},
            {i: '3', x: 4, y: 0, w: 3, h: 6}
        ];
        let highChartOption = {
            chart: {
                type: 'column'
            },
            title: {
                text: 'chart 1'
            },
            xAxis: {
                categories: ['my', 'first', 'chart']
            },
            yAxis: {
                title: {
                    text: 'something'
                }
            },
            series: [{
                name: 'Jane',
                data: [1, 0, 4]
            }, {
                name: 'John',
                data: [5, 7, 3]
            }],
            credits: {
                enabled: false
            }
        };
        return (
            <ReactGridLayout className="layout"
                             layout={layout} cols={12} rowHeight={30} width={1920} >
                <div key={'1'} className="greenBox">a</div>
                <div key={'2'} className="greenBox">b</div>
                <div key={'3'} className="greenBox" ref="chartContainer">
                    <ReactHighcharts config={highChartOption}
                                     ref="chart"
                                     domProps={{style: {width: '100%', height: '100%'}}}/>
                </div>
            </ReactGridLayout>
        )
    }
});

ReactDom.render(<SecondGrid />, document.getElementById("renderBox"));

